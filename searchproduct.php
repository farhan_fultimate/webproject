<?php 
	include('php/database.php');
	if(isset($_GET['data'])){
		$searchdata = $connect->real_escape_string($_GET['data']);
		$sql = "SELECT * FROM store WHERE itemname LIKE  '%".$searchdata."%' AND flag <>0;";
		//echo $sql;
		$res = $connect->query($sql);
		$data = array();
		while($row = $res->fetch_assoc()){
			$temp = array(
					'sid'=>$row['sid'],
					'id'=>$row['id'],
					'username'=>$row['username'],
					'itemname'=>$row['itemname'],
					'price'=>$row['price'],
					'categories'=>$row['categories'],
					'itemtype'=>$row['itemtype'],
					'address'=>$row['address'],
					'tel'=>$row['tel'],
					'detail'=>$row['detail'],
					'images'=>$row['images'],
					'date'=>$row['date']
				);
			array_push($data , $temp);
		}
	    echo json_encode($data);
	}
	else{
		echo "none";
	}
	
?>