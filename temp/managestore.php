<?php
  session_start();

?>
<html lang="en" class=" "><head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Gentallela Alela! | </title>
  <!-- Bootstrap -->
  <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
  <!-- Font Awesome -->
  <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="screen" href="css/ionicons.min.css?v=2.0.1">

  <!-- Custom Theme Style -->
  <link href="production/css/custom.css" rel="stylesheet">
  <script src="vendors/jquery/dist/jquery.min.js"></script>
  <script src="js/indexjs.js"></script>
</head>
<body class="nav-md" ng-app="indexApp" ng-controller="indexCtrl">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <a href="index.html" class="site_title"><i class="fa fa-globe"></i> <span>Kaigame Shop</span></a>
          </div>
          <div class="clearfix"></div>
          <br>
          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
              <h3>Catagaries</h3>
              <ul class="nav side-menu">
                <li><a><i class="ion-social-windows"></i> แผ่นเกมมือสอง <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                  <li id="xboxdisk" ng-click="clickNfade('แผ่นXBOX')">
                    <a><span class="ion-xbox"></span>   XBOX</a>
                  </li>
                  <li id="pcdisk"  ng-click="clickNfade('แผ่นเกมPC')">
                    <a><span class="ion-social-windows"></span>   PC</a>
                  </li>
                  <li  ng-click="clickNfade('แผ่นเกมPLAYSTATION')">
                    <a><span class="ion-playstation"></span>   PLAYSTATION</a>
                  </li>
                  <li ng-click="clickNfade('แผ่นเกมNINTENDO')">
                    <a> <span class="ion-ios-game-controller-a-outline"></span>   NINTENDO</a>
                  </li>
                </ul>
              </li>
              <li ><a><i class=" fa fa-gamepad"></i> เครื่องเล่นเกมมือสอง <span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li>
                  <a><span class="ion-playstation"></span>   PLAYSTATION</a>
                </li>
                <li>
                  <a><span class="ion-xbox"></span>   XBOX</a>
                </li>
                <li>
                  <a> <span class="ion-ios-game-controller-a-outline"></span>   NINTENDO</a>
                </li>
              </ul>
            </li>
            <li><a><i class="fa fa-desktop"></i> อุปกรณ์ console/PC <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li>
                <a>อุปกรณ์PC</a>
              </li>
              <li>
                <a>อุปกรณ์Console</a>
              </li>
            </ul>
          </li>
          <li><a><i class="fa fa-book"></i> หนังสือ <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li>
              <a>หนังสือการ์ตูน</a>
            </li>
          </ul>
        </li>
        <li><a><i class="fa fa-server"></i> แผ่นDVD <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li>
            <a>อนิเมะ</a>
          </li>
          <li>
            <a>ภาพยนตร์</a>
          </li>
        </ul>
      </li>
      <li><a><i class="fa fa-clone"></i> ของเล่นและของสะสม <span class="fa fa-chevron-down"></span></a>
      <ul class="nav child_menu">
        <li>
          <a>ตัวฟิกเกอร์</a>
        </li>
      </ul>
    </li>
     <li><a><i class="fa fa-desktop"></i> Admin <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li>
                <a>จัดการผู้ใช้</a>
              </li>
              <li>
                <a>จัดการสินค้า</a>
              </li>
            </ul>
          </li>
  </ul>
</div>
</div>
<!-- /sidebar menu -->
<!-- /menu footer buttons -->
<!-- <div class="sidebar-footer hidden-small">
<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Settings">
  <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
</a>
<a data-toggle="tooltip" data-placement="top" title="" data-original-title="FullScreen">
  <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
</a>
<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Lock">
  <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
</a>
<a data-toggle="tooltip" data-placement="top" title="" data-original-title="Logout">
  <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
</a>
</div> -->
<!-- /menu footer buttons -->
</div>
</div>
<!-- top navigation -->
<?php
  if(!isset($_SESSION['user'])){
            echo "<div class='top_nav'>
          <div class='nav_menu'>
          <nav class='' role='navigation'>
          <div class='nav toggle'>
            <a id='menu_toggle'><i class='fa fa-bars'></i></a>
          </div>
          <ul class='nav navbar-nav navbar-right'>
            <li class=''>
              <a href='login.html' >
                <span class='glyphicon glyphicon-log-in' aria-hidden='true'></span>  เข้าสู่ระบบ
              </a>
            </li>
            <li class=''>
              <a href='login.html'>
                <span class='glyphicon glyphicon-registration-mark' aria-hidden='true'></span>  สมัครสมาชิก
              </a>
            </li>
          </ul>
          </nav>
          </div>
          </div>";
  }
  else{
          echo "<div class='top_nav'>
          <div class='nav_menu'>
          <nav class='' role='navigation'>
          <div class='nav toggle'>
            <a id='menu_toggle'><i class='fa fa-bars'></i></a>
          </div>
          <ul class='nav navbar-nav navbar-right'>
            <li class=''>
                  <a href='javascript:;'' class='user-profile dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>
                    <img src='images/john.jpg' alt=''> ".$_SESSION['user']."
                    <span class='fa fa-angle-down'></span>
                  </a>
                  <ul class='dropdown-menu dropdown-usermenu pull-right'>
                  <li><a href='users.php'><i class='fa fa-list-alt pull-right'></i> ไปยังหน้าสินค้า</a>
                    </li>
                    <li><a href='php/logout.php'><i class='fa fa-sign-out pull-right'></i> ออกจากระบบ</a>
                    </li>

                  </ul>
            </li>
            
          </ul>
          </nav>
          </div>
          </div>";
  }
?>

<!-- /top navigation -->
<!-- page content -->
<div class="right_col" role="main" style="min-height: 944px;">
<div class="">
<div class="page-title">
<div class="title_left">
  <h3>แผ่นเกมมือสอง</h3>
</div>
<div class="title_right">
  <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
    <div class="input-group">
      <input type="text" class="form-control" placeholder="Search for...">
      <span class="input-group-btn">
        <button class="btn btn-default" type="button">Go!</button>
      </span>
    </div>
  </div>
</div>
</div>
<div class="clearfix"></div>
<div id="box" class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel" style="height:600px;">
    <div class="x_title">
      <h2 id="titleText" ng-model="titleText">{{titleText}}</h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
      </li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
        <ul class="dropdown-menu" role="menu">
          <li><a href="#">Settings 1</a>
        </li>
        <li><a href="#">Settings 2</a>
      </li>
    </ul>
  </li>
  <li><a class="close-link"><i class="fa fa-close"></i></a>
</li>
</ul>
<div class="clearfix"></div>
</div>
<div class="container-fluid">

    <table class="table table-striped" id="demo" >
      <tr>
        
        <td>id</td>
        <td>username</td>
        <td>itemname</td>
        <td>price</td>
        <td>categories</td>
        <td>item type</td>
        <td>e-mail</td>
        <td>tel.</td>
        <td>address</td>
        <td>image</td>
        <td>date</td>
        <td>delete</td>
      </tr> 
      <tr ng-repeat="d in data">
        
        <td>{{d.id}}</td>
        <td>{{d.username}}</td>
        <td>{{d.itemname}}</td>
        <td>{{d.price}}</td>
        <td>{{d.categories}}</td>
        <td>{{d.itemtype}}</td>
        <td>{{d.email}}</td>
        <td>{{d.tel}}</td>
        <td>{{d.address}}</td>
        <td><img src="images/id{{d.sid}}_1.jpeg" class="img-responsive" alt="image" style="width: auto; height: auto; display: block; max-height: 300px; max-height: 150px;"></td>
        <td>{{d.date}}</td>
        <td><button data = "{{d.sid}}" class="ion-trash-a" type="button" ng-click="del(d.sid)"></button></td>
      <tr>
    </table>


     
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<footer>
<div class="pull-right">
Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
</div>
<div class="clearfix"></div>
</footer>
</div>
</div>
<!-- jQuery -->
<!-- Bootstrap -->
<script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="vendors/nprogress/nprogress.js"></script>
<!-- Custom Theme Scripts -->
<script src="production/js/custom.js"></script>



<script>
  var app = angular.module('indexApp', []);
  app.controller('indexCtrl', function($scope,$http){

        $http.get("php/getstore.php").then(function(response) {
            $scope.data = response.data;
        });
        $scope.del = function(str){

         
          $http.get("php/deletestore.php?sid="+str).then(function(response){
          $scope.data = response.data;
          $scope.loadData();
        });
        };
       $scope.loadData = function(){
            $http.get("php/getstore.php").then(function(response) {
            $scope.data = response.data;
        });
       };
  }
  );

</script>
</body></html>