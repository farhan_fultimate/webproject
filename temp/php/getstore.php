<?php 
	include('database.php');
	
	$getstore = "SELECT * FROM store WHERE 1=1;";
	
	$res = $connect->query($getstore);
	$data = array();
	while($row = $res->fetch_assoc()){
		$temp = array(
				'sid'=>$row['sid'],
				'id'=>$row['id'],
				'username'=>$row['username'],
				'itemname'=>$row['itemname'],
				'price'=>$row['price'],
				'categories'=>$row['categories'],
				'itemtype'=>$row['itemtype'],
				'address'=>$row['address'],
				'tel'=>$row['tel'],
				'detail'=>$row['detail'],
				'images'=>$row['images'],
				'date'=>$row['date']
			);
		array_push($data , $temp);
	}
    echo json_encode($data);
 ?>