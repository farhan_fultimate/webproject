<?php
require('fpdf.php');

class PDF extends FPDF
{
function Header()
{
    global $title;

    $this->Image('logo.jpg',10,6,30);
	// Arial bold 15
	
	// Move to the right
	$this->Cell(80);
    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Calculate width of title and position
    $w = $this->GetStringWidth($title)+6;
    $this->SetX((210-$w)/2);
    // Colors of frame, background and text
    $this->SetDrawColor(255,255,255);
    $this->SetFillColor(255,255,255);
    $this->SetTextColor(0,0,0);
    // Thickness of frame (1 mm)
    $this->SetLineWidth(1);
    // Title
    $this->Cell($w,9,$title,1,1,'C',true);
    // Line break
    $this->SetFont('Arial','',12);
    $this->SetX((214-$w)/2);
    $this->Cell(0,5,'Course:Basic Internetworking');
    $this->Ln(20);
}


function Footer()
{	
	$date = date("d-m-Y"); 
    $heure = date("H:i"); 
    
    // Position at 1.5 cm from bottom
    $this->SetY(-100);
    // Arial italic 8
    $this->SetFont('Arial','U',8);
     //$this->Cell(0,5,"$date");
     //$this->Ln(4);$this->Cell(0,5,"$heure");
    $this->Ln(50);
    // Text color in gray
    $this->Cell(0,5,"Sign:                                                                                                                                                                             date: $date     time: $heure");
   // $pdf->Text(100,100,.$date." ".$heure);
    $this->SetTextColor(128);


    // Page number
    //$this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
}

function ChapterTitle($label)
{
	//if($num==1){
    // Arial 12
    $this->SetFont('Arial','',12);

    // Background color
    $this->SetFillColor(200,220,255);
    // Title 

    $this->Cell(0,6," $label",0,1,'C',true);
   
    // Line break
    $this->Ln(4);
	//}	
}

function ChapterBody()
{
    // Read text file
  //  $txt = file_get_contents($file);
    // Times 12
     $this->SetFont('Times','',12);
     $this->Cell(30,6,"Initial:",0,0);
     $this->SetFont('Times','U',12);
     $this->Cell(20,6,"Mr",0,0);
   
     $this->SetFont('Times','',12);
     $this->Cell(30,6,"Firstname:",0,0);
     $this->SetFont('Times','U',12);
     $this->Cell(30,6,"aa",0,0);
    
     $this->SetFont('Times','',12);
     $this->Cell(30,6,"Lastname:",0,0);
     $this->SetFont('Times','U',12);
     $this->Cell(30,6,"aa",0,1);
     $this->Ln(4);

     $this->SetFont('Times','',12);
     $this->Cell(30,6,"Organization:",0,0);
     $this->SetFont('Times','U',12);
     $this->Cell(30,6,"aa",0,0);
    
     $this->SetFont('Arial','',12);
     $this->Ln(10);
    // Background color
    $this->SetFillColor(200,220,255);
    // Title 
    $this->Ln(4);

    $this->Cell(0,6,"Registration",0,1,'L',true);
    

     $this->SetFont('Times','',12);
     $this->Cell(30,6,"Status:",0,0);
     $this->SetFont('Times','U',12);
     $this->Cell(30,6,"Staff",0,0);

     $this->SetFont('Times','',12);
     $this->Cell(30,6,"Education:",0,0);
     $this->SetFont('Times','U',12);
     $this->Cell(30,6,"Undergrad",0,1);
     $this->Ln(4);

     $this->SetFont('Times','',12);
     $this->Cell(30,6,"Department:",0,0);
     $this->SetFont('Times','U',12);
     $this->Cell(30,6,"EGCO",0,1);
     $this->Ln(4);
     
     $this->SetFont('Times','',12);
     $this->Cell(30,6,"Training:",0,0);
     $this->SetFont('Times','U',12);
     $this->Cell(50,6,"Basic Internetworking",0,0);

     $this->SetFont('Times','',12);
     $this->Cell(30,6,"Fee:",0,0);
     $this->SetFont('Times','U',12);
     $this->Cell(30,6,"2000 Baht",0,0);
   
 
    // Line break
    $this->Ln();
    // Mention in italics
    $this->SetFont('','I');
   
}

function PrintChapter($title)
{
    $this->AddPage();
    $this->ChapterTitle($title);
    $this->ChapterBody();
}
}

$pdf = new PDF();

$title = 'Cisco Network Academic Training@EGCO';
$pdf->SetTitle($title);

$pdf->PrintChapter('REGISTRATION FORM');


//$pdf->PrintChapter(2,'Registration');
$pdf->Output();
?>  