<?php 
	include('database.php');
	if(isset($_GET['amphur'])){
		$provinceid = $_GET['amphur'];
		$getamphur = "SELECT AMPHUR_NAME ,AMPHUR_ID FROM tbl_amphur WHERE PROVINCE_ID=".$provinceid.";";
		$res = $connect->query($getamphur);
		$data = array();
		while($row = $res->fetch_assoc()){
			$temp = array(
				'id'=>$row['AMPHUR_ID'],
				'name'=>$row['AMPHUR_NAME']
			);
			array_push($data , $temp);
		}
		echo json_encode($data);
	}
	else{
		$getprovince = "SELECT * FROM tbl_PROVINCE WHERE 1=1; ";
		$res = $connect->query($getprovince);
		$data = array();
		while($row = $res->fetch_assoc()){
			$temp = array(
					'id'=> $row['PROVINCE_ID'],
					'name'=> $row['PROVINCE_NAME']
				);
			array_push($data , $temp);
		}
		echo json_encode($data);
	}
 ?>